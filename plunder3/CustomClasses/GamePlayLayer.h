//
//  GamePlayLayer.h
//  plunder3
//
//  Created by Yong-uk Choe on 13. 2. 18..
//  Copyright __MyCompanyName__ 2013년. All rights reserved.
//


#import <GameKit/GameKit.h>
#import <Foundation/Foundation.h>
#import "cocos2d.h"

#define P3_GROUNDHEIGHT 50

// GamePlayLayer
@interface GamePlayLayer : CCLayer //<GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>
{
  // screen width, height
  CGSize _screen;
  
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
