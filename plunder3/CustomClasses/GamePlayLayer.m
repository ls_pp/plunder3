//
//  GamePlayLayer.m
//  plunder3
//
//  Created by Yong-uk Choe on 13. 2. 18..
//
//

#import "GamePlayLayer.h"
#import "AppDelegate.h"

#import "PlayerUnitSprite.h"

#pragma mark - GamePlayLayer

// GamePlayLayer implementation
@implementation GamePlayLayer

+(CCScene *) scene {
	CCScene *scene = [CCScene node];
	GamePlayLayer *layer = [GamePlayLayer node];
	[scene addChild: layer];

	return scene;
}

-(id) init {
	if( (self=[super init]) ) {
    _screen = [[CCDirector sharedDirector] winSize];

    PlayerUnitSprite *p_test = [PlayerUnitSprite node];
    [self addChild:p_test];
    [p_test walk];
	}
	return self;
}

- (void)draw {
  
  // ground benchmark line
  ccDrawLine( ccp(0, P3_GROUNDHEIGHT), ccp(_screen.width, P3_GROUNDHEIGHT) );
}

- (void) dealloc {

	[super dealloc];
}

@end
