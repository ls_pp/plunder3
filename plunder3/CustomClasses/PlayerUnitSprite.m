//
//  PlayerUnitSprite.m
//  plunder3
//
//  Created by Yong-uk Choe on 13. 2. 19..
//  Copyright 2013년 __MyCompanyName__. All rights reserved.
//

#import "PlayerUnitSprite.h"


@implementation PlayerUnitSprite

-(id) init {
  if (self = [super initWithFile:@"walk-right0.png"]) {
    //CGSize screen = [CCDirector sharedDirector].winSize;
    
    self.position = P3_PlayerUnitSpriteStartingPoint;
    
    _status = PlayerUnitSpriteStatus_WAIT;
    
    CCAnimation *animation = [[CCAnimation animation] retain];
    [animation addSpriteFrame:[CCSpriteFrame frameWithTextureFilename:@"walk-right0.png" rect:CGRectMake(0, 0, 33, 33)]];
    [animation addSpriteFrame:[CCSpriteFrame frameWithTextureFilename:@"walk-right1.png" rect:CGRectMake(0, 0, 33, 33)]];
    [animation addSpriteFrame:[CCSpriteFrame frameWithTextureFilename:@"walk-right2.png" rect:CGRectMake(0, 0, 33, 33)]];
    [animation setDelayPerUnit:0.5f];

    id moveOne = [CCMoveBy actionWithDuration:1.5f position:ccp(33, 0)];

    _walkAnimation = [CCRepeatForever actionWithAction:
                      [CCSequence actions:
                       [CCSpawn actionOne:moveOne
                                     two:[CCAnimate actionWithAnimation:animation]],
                       [CCCallBlock actionWithBlock:^{ if (self.position.x > 200) [self stop]; }], nil]
                     ];
                      
    
  }
  return self;
}

-(void) walk {
  [self runAction:_walkAnimation];
  _status = PlayerUnitSpriteStatus_WALK;
}

-(void) stop {
  [self stopAction:_walkAnimation];
  _status = PlayerUnitSpriteStatus_WAIT;
}

@end
