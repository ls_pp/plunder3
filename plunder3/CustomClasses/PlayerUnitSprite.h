//
//  PlayerUnitSprite.h
//  plunder3
//
//  Created by Yong-uk Choe on 13. 2. 19..
//  Copyright 2013년 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#import "P3_config.h"

typedef enum {
  PlayerUnitSpriteStatus_WAIT,
  PlayerUnitSpriteStatus_WALK,
  PlayerUnitSpriteStatus_FIGHT
} PlayerUnitSpriteStatusType;

@interface PlayerUnitSprite : CCSprite {
  PlayerUnitSpriteStatusType _status;
  CCAction *_walkAnimation;
  float _walkSpeed;
}

-(void) walk;
-(void) stop;


@end
